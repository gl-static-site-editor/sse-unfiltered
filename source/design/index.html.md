---
title: Static Site Editor Design
layout: page
---

# Design changelog

## 2020-08-12

### Solution validation for publishing workflow

Here are [the insights from this solution validation ](https://dovetailapp.com/projects/1bc79616-0aa4-4d33-89a3-bc83c32f8678/insights/present)

## 2020-04-06

### Designer joins the team

Michael Le joins the Static Site Editor team