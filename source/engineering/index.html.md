---
title: Engineering Changelog
layout: page
---

# Engineering changelog

## 2020-10-15

### Class and markup cleanup to prevent SVG header bar overlap

Fix minor mobile UX overlap bug.

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45334](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/45334)

## 2020-10-08

### Add support for new SSE Config File

Adds new Static Site Editor configuration file with three initial entries.

[https://gitlab.com/groups/gitlab-org/-/epics/4267#issues-and-mrs-to-implement-this-epic](https://gitlab.com/groups/gitlab-org/-/epics/4267#issues-and-mrs-to-implement-this-epic)

## 2020-10-06

### Provide optional title and description before submitting edits w/SSE

Optional title and description UI addition.

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/44512](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/44512)

## 2020-09-16

### Add front matter UI context for 13.4 release

Adds documentation context around the front matter UI feature landing in 13.4.

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42513](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42513)

## 2020-09-16

### Simple gitlab-ui class use update

Simple css class update to leverage gitlab-ui util.

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42535](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42535)

## 2020-09-15

### Add `gl-pt-8` for downstream use in Static Site Editor

Simply adds the `gl-pt-8` spacing utility.

[https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1721](https://gitlab.com/gitlab-org/gitlab-ui/-/merge_requests/1721)

## 2020-09-09

### Wire-up EditDrawer with FrontMatterControls

Updates the UI so a `GlDrawer` encapsulates the front matter controls as originally designed.

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41920](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41920)

## 2020-09-04

### Resolve "Add front_matter_controls.vue component"

Add the v1 of front matter controls where the next iteration will ensure they live in a drawer component

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41613](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41613)

## 2020-09-04

### Minor bottom spacing add to align with top spacing

Minor UX spacing fix

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41596](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41596)

## 2020-09-02

### Use gray-matter vs. prev approach at MR noise tradeoff

Add initial front matter parsing solution via `gray-matter`

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41230](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41230)

## 2020-08-28

### DRY mock data cleanup

Mini tech debt cleanup

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40775](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40775)

## 2020-08-28

### Updated SSE front matter language support

Initial iteration for non-YAML front matter support (TOML, JSON)

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40718](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40718)

## 2020-08-25

### Resolve "Add front matter read/write API to parse_source_file.js"

Update `parse_source_file` to have a read/write API to prepare downstream MRs

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40411](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/40411)

## 2020-08-07

### Iterate on `templater` pre-processing step handle inline ERB and HTML

Iteration to improve what the `templater` can handle

| before | after |
| ------ | ----- |
| ![Screen_Shot_2020-08-06_at_9.06.20_AM](https://gitlab.com/gitlab-org/gitlab/uploads/14ea65a3270a60e84c83140e612ed98a/Screen_Shot_2020-08-06_at_9.06.20_AM.png) | ![Screen_Shot_2020-08-06_at_8.05.24_AM](https://gitlab.com/gitlab-org/gitlab/uploads/93f1e16ef9bd95bc553b2fbd1bc697a2/Screen_Shot_2020-08-06_at_8.05.24_AM.png) |

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38791](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38791)

## 2020-08-06

### Add `templater` pre-processing step to flag template code

We added a pre-processing `templater` step to ensure WYSIWYG mode preserves template code (embedded ruby currently) instead of interpreting it as Markdown. Markdown mode undoes this step so source code saving still works as expected.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38694](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/38694)

## 2020-08-03

### Feature: Improved the look & feel of the success screen

We have improved the visual appearance of the success screen in the Static Site Editor
![https://i.ibb.co/KV95J1H/Screen-Shot-2020-08-03-at-10-15-06-AM.png](https://i.ibb.co/KV95J1H/Screen-Shot-2020-08-03-at-10-15-06-AM.png)

### Feature: Add "Edit page" links to the right sidebar in the Handbook

We’ve made easier to edit handbook pages by displaying edit links in the top of the Handbook‘s navigation sidebar.
![https://i.ibb.co/8d8Q05M/Screen-Shot-2020-08-03-at-10-20-46-AM.png](https://i.ibb.co/8d8Q05M/Screen-Shot-2020-08-03-at-10-20-46-AM.png)

### Various bug fixes

- Ordered lists were incrementing the list marker after the 9th list item.
- Italics and bold text format was broken if there was a soft break within the formatted text.

## 2020-07-29

### Remove extraneous br tags via formatter

We added a formatting step which currently is used to remove unwanted duplicate `<br>` tags generated by Toast UI's use of Squire.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37223](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37223)

## 2020-07-27

### Markdown generation preferences tweaks

We’ve made the following changes in the way the Static Site Editor generates Markdown from the content created in WYSIWYG mode:

- The editor will use `**` for strong text and `_`for emphasis.
- The editor won’t auto-increment the list marker of ordered lists, for example:

```
<!-- instead of this --> 
1. first item
2. second item
3. third item
<!-- it will do this -->
1. first item
1. second item
1. third item
```

- The editor will use `-` as the default list marker for unordered lists.

## 2020-07-16

### Add render_utils.js for DRY-sake

DRY refactoring
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37129](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37129)

### Uneditable block spacing and minor listener refactor

Gives uneditable blocks vertical margin so it breathes better in relation to other WYSIWYG content
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37087](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/37087)

## 2020-07-15

### Resolve "Display non-markdown content in the WYSIWYG mode of the SSE::HTML"

Add html custom renderer.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36330](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36330)

### Resolve "Display non-markdown content in the WYSIWYG mode of the SSE::Identifiers (inline instance)"

Add identifier instances custom renderer
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36574](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36574)

## 2020-07-14

### Add font awesome custom renderer

Add _font awesome_ custom renderer.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36361](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36361)

## 2020-07-07

### Fix: Static Site Editor link in the Handbook

The Static Site Editor link in the handbook product section was broken.
[https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/55120](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/55120)

### Fix: Incorrect text escaping

When editing a markdown document that contains custom Markdown syntax like Kramdown or GFM with the Static Site Editor, Toast UI (the underlying editor used by the SSE) applies text escaping to the markdown syntax.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35671](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35671)

## 2020-06-29

### Fix: Can’t open Static Site Editor in projects that belong to a subgroup

[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35378](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35378)

### Allow files with `.md.erb` extension in the Static Site Editor

It extends Static Site Editor functionality to support more file extensions.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35136](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35136)

## 2020-06-24

### Add a custom HTML renderer to the Static Site Editor for embedded ruby (ERB) syntax

When the user edits a page with ERB code syntax, they are unable to edit it in WYSIWYG mode.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35261](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35261)

## 2020-06-23

### Add upload file tab to the SSE image popup

This adds an upload file tab to the Static Site Editor insert image popup and displays the image in the WYSIWIG editor with a temporary URL.
It's the 1st iteration towards supporting image uploads.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35126](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35126)

## 2020-06-22

### Add a custom HTML renderer to the Static Site Editor for markdown identifier syntax

When the user tries to edit a page that contains identifier syntax, they are prevented from editing in WYSIWYG mode.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35077](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/35077)

## 2020-06-11

### Update Static Site Editor WYSIWYG mode to hide front matter

When the user tries to leave the Static Site Editor, display a confirmation dialog indicating that they may lose their changes.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33441](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33441)

## 2020-06-09

### Display a confirmation modal before leaving the editor

When the user tries to leave the Static Site Editor, display a confirmation dialog indicating that they may lose their changes.
[https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33103](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33103)