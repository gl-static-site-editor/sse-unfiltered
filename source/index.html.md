---
title: Static Site Editor Unfiltered
layout: page
---

# Hello world

This is a space for GitLab's Static Site Editor group to share behind-the-scenes, unfiltered details about the direction and development of our feature.

- [What's the Static Site Editor?](direction)
- [Product changelog](product)
- [Engineering changelog](engineering)
- [Design changelog](design)
