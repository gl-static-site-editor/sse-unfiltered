---
title: Static Site Editor Direction
layout: page
---
# Direction

## What is the Static Site Editor?

The Static Site Editor was born out of the necessity for collaboration. At GitLab, we believe that everyone can contribute, not just everyone who knows how to pull a branch from the git command line or navigate a file tree in the Web IDE.

Our goal is to provide an environment for editing content on static sites that feels familiar, intuitive, and efficient. One that removes as many barriers to contribution as possible.